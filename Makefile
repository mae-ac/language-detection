.PHONY: test q1 q2 q3 q4

test:
	pytest

q1:
	python src/occurrence_frequencies.py

q2:
	python src/enhanced_occurrence_frequencies.py

q3:
	python src/text_length.py

q4:
	python src/data_compression.py
