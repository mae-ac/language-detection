from tests.constants import LANGUAGE_FILES, PRECISION, ASCII_ALPHABET

ENTROPIES = {
    "french": 4.209560977492212,
    "english": 4.36312826556579,
    "german": 4.213059203708274,
    "spanish": 4.186034261730856,
}


def test():
    from src.data_compression import entropy
    from src.enhanced_occurrence_frequencies import (
        read_text, compute_occurrence_frequencies
    )

    for lang, file in LANGUAGE_FILES.items():
        e = entropy(
            compute_occurrence_frequencies(read_text(file), ASCII_ALPHABET)
        )
        expected = ENTROPIES[lang]
        assert abs(e - expected) <= PRECISION, (
            f"entropy should be closer to {expected}, found {e}, delta "
            f"= {abs(e - expected)}"
        )
