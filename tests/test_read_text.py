def test_read_text():
    from src.enhanced_occurrence_frequencies import read_text

    for (file, expected, msg) in [
        ("tests/sample.txt", "this is a sample text file\n", None),
        (
            "tests/empty.txt",
            "",
            "`read_text` should give an empty string for an empty file",
        ),
        (
            "tests/multiline.txt",
            "i\nam\na\nmultiline\ntext\n",
            "`read_text` should handle newlines",
        ),
        (
            "tests/symbols.txt",
            "this is a (sample) text, containing (a lot of) punctuation!!\n",
            "`read_text` should only make the text lower case, not remove symbols",
        ),
    ]:
        if msg is not None:
            assert read_text(file) == expected, msg
        else:
            assert read_text(file) == expected
