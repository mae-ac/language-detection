from tests.constants import ALPHABET, LANGUAGE_FILES


def test_detect_language():
    from src.enhanced_occurrence_frequencies import (
        compute_occurrence_frequencies, read_text, detect_language
    )

    ofs = {
        lang: compute_occurrence_frequencies(read_text(file), ALPHABET)
        for lang, file in LANGUAGE_FILES.items()
    }

    for lang, file in LANGUAGE_FILES.items():
        assert detect_language(read_text(file), ofs, ALPHABET) == lang
