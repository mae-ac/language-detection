from tests.constants import ALPHABET


def test_type():
    from src.enhanced_occurrence_frequencies import compute_occurrence_frequencies

    actual = compute_occurrence_frequencies("a", ALPHABET)
    assert isinstance(actual, dict), (
        "result of `compute_occurrence_frequencies` should be a dictionary, "
        f"found {type(actual).__name__}"
    )
    assert sorted(actual.keys()) == sorted(list(ALPHABET))


def test_output():
    from src.enhanced_occurrence_frequencies import compute_occurrence_frequencies

    actual = compute_occurrence_frequencies(
        "this is some random text", ALPHABET
    )
    assert actual == {
        'a': 0.05,
        'b': 0.0,
        'c': 0.0,
        'd': 0.05,
        'e': 0.1,
        'f': 0.0,
        'g': 0.0,
        'h': 0.05,
        'i': 0.1,
        'j': 0.0,
        'k': 0.0,
        'l': 0.0,
        'm': 0.1,
        'n': 0.05,
        'o': 0.1,
        'p': 0.0,
        'q': 0.0,
        'r': 0.05,
        's': 0.15,
        't': 0.15,
        'u': 0.0,
        'v': 0.0,
        'w': 0.0,
        'x': 0.05,
        'y': 0.0,
        'z': 0.0,
    }
