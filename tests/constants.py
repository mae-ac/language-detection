ALPHABET = "abcdefghijklmnopqrstuvwxyz"
ASCII_ALPHABET = [chr(i) for i in range(128)]

LANGUAGE_FILES = {
    "french": "data/french_madame_bovary.txt",
    "english": "data/english_wuthering_heights.txt",
    "german": "data/german_kritik_der_reinen_vernunft.txt",
    "spanish": "data/spanish_don_quijote.txt",
}

PRECISION = 1e-5
